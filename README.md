# Vincent Tunru

Hi there! I've got a bunch of projects, most of them unfinished. However, you can find the interesting, usable ones, over at **[gitlab.com/VincentTunru](https://gitlab.com/VincentTunru/)**.

Also, you can find me elsewhere:

- 🌐 https://vincenttunru.com/
- 🐘 https://fosstodon.org/@VincentTunru
- 🐦 https://twitter.com/VincentTunru
- 🐙 https://github.com/Vinnl/
